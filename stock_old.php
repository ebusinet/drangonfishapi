<?php
require_once 'vendor/autoload.php';
require_once 'Dragonfish.php';
require_once 'UConnector.php';
use Firebase\JWT\JWT;

const SYNC_NAME = 'stock';
const COL_PLATFORM_ID = 0;
const COL_SKU = 1;
const COL_QTY = 2;

$start = microtime(true);
echo SYNC_NAME." data sync start. \n";

$dragonfish = new Dragonfish();
$jwt = $dragonfish->getAuthToken();
if ($jwt){
	echo "Retriving ".SYNC_NAME." data from ERP. \n";
 	$data = $dragonfish->getDataFromErp(SYNC_NAME);
}

if (!empty($data)) {
	$uConnector = new UConnector();
	$jwt = $uConnector->getAuthToken();
	if ($jwt){
		echo "Sending ".SYNC_NAME." data to UConnector. \n";
		for ($i=0; $i < count($data); $i++) { 
			$response = $uConnector->sendData(
				[
					'plataform_id_to' => $data[$i][COL_PLATFORM_ID],
					'sku' => $data[$i][COL_SKU], 
					'qty'=> $data[$i][COL_QTY],
				]
			);
			if ($response['status'] == 201) {
				$data[$i][SYNC_NAME.'_sync_id'] = $response['data']['id'];
			} else {
				$data[$i][SYNC_NAME.'_sync_id'] = $response['content'];
			}
		}
		$timestamp = date('Y-m-d_H_i_s', time());
		array_unshift($data,['plataform_id_to', 'sku', 'qty', SYNC_NAME.'_sync_id']);
		$dragonfish->createCSV(SYNC_NAME, $timestamp, $data);
	} else {
		echo "Error requesting token. \n";
	}
}

echo SYNC_NAME." data sync finished. \n";
$time_elapsed_secs = microtime(true) - $start;
echo "Time to execute: ".$time_elapsed_secs ." seconds \n";