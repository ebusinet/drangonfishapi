<?php
require_once 'vendor/autoload.php';
require_once 'Dragonfish.php';
require_once 'UConnector.php';

use Firebase\JWT\JWT;
use IDCT\Networking\Ssh\SftpClient;
use IDCT\Networking\Ssh\Credentials;

const SYNC_NAME = 'order';
const DIR_PENDING = SYNC_NAME.DIRECTORY_SEPARATOR.'pending'.DIRECTORY_SEPARATOR;
const DIR_PROCESSED = SYNC_NAME.DIRECTORY_SEPARATOR.'processed'.DIRECTORY_SEPARATOR;

$start = microtime(true);
echo SYNC_NAME." order sync start. \n";
echo "Retriving ".SYNC_NAME." data from uconnector. \n";

try {
	$remotePath = '/import/order/pending/';
	$remotePath2 = '/import/order/processed/';

	$host = 'uconnector.ulula.net';
	$port = 22;
	$timeout = 10;
	$user = 'spaceball';
	$pass = 'spaceball2021';
	$client = new SftpClient();
	$credentials = Credentials::withPassword($user, $pass);
	$client->setCredentials($credentials);

	$client->connect($host);
	
	$files = $client->getFileList($remotePath);
	foreach ($files as $file) {
		if (strlen($file) < 3) continue;
		
		$remoteFilePath = $remotePath.$file;
		$downloaded = $client->download($remoteFilePath, DIR_PENDING.$file);

		$newRemoteFilePath = '/import/order/processed/'.date('m-d-Y_H:i:s').'_'.$file;
		$client->rename($remoteFilePath, $newRemoteFilePath);
	}

	$client->close();
	echo "Orders downladed. \n";
	
} catch (\Exception $e) {
	var_dump($e->getMessage());
	$client->close();
	die;
}



$dragonfish = new Dragonfish();
$jwt = $dragonfish->getAuthToken();

if ($jwt){
	if ($handle = opendir(DIR_PENDING)) {

	    while (false !== ($entry = readdir($handle))) {

	        if ($entry != "" && $entry != "." && $entry != "..") {

	        	echo "Processing order ".$entry. "\n";
	        	$order = file_get_contents(DIR_PENDING.$entry); 

	 			$data = $dragonfish->createOrder($order);
	 			if ($data) {
	 				$oldName = DIR_PENDING.$entry;
		 			$newName = DIR_PROCESSED.$entry;
		 			rename($oldName, $newName);
	 			}
	        }
	    }

	    closedir($handle);
	}
}

echo SYNC_NAME." data sync finished. \n";
$time_elapsed_secs = microtime(true) - $start;
echo "Time to execute: ".$time_elapsed_secs ." seconds \n";