<?php
require_once 'vendor/autoload.php';
require_once 'config.php';
use Firebase\JWT\JWT;

/**
 * 
 */

class UConnector
{	
	const AUTH_URL = 'auth/login';
	const STOCK_URL = 'stocksync';
	const PRICE_URL = 'pricesync';

	protected $key;
	protected $user;
	protected $password;
	protected $url;
	protected $jwToken;
	protected $authToken;
	
	function __construct()
	{
		$config = Config::getConfig();
		$this->key = $config['connector']['secret_key'];
		$this->user = $config['connector']['user'];
		$this->password = $config['connector']['password'];
		$this->url = $config['connector']['host'].':'.$config['connector']['port'].'/api/v1/';
	}

	public function getJWToken()
	{
		return JWT::encode($this->getToken(), $this->key);
	}

	public function getToken()
	{
		$time = time();
		return [
		    'exp' => $time + (60*60), 
		    'user' => $this->user,
		    'password' => $this->password
		];
	}

	protected function getHeaders()
	{
		$headerToken = ($this->authToken) ? 'Authorization: Bearer '.$this->authToken : '';
		return [
			'Content-type' => 'application/json',
			'Accept' => 'application/json'
		];
	}

	protected function getAuthBody()
	{	
		return json_encode($this->getJWToken());
	}

	public function getAuthToken()
	{
		$url = $this->url.self::AUTH_URL;
		if (!$this->authToken) {
			try{
				$response = CurlHelper::factory($url)
					->setHeaders($this->getHeaders())
					->setPostRaw($this->getAuthBody())
					->exec();
				if ($response['status'] == 200) {
					$this->authToken = $response['data']['token'] ;
				}
			} catch(Exception $e) {
				print_r($e->getMessage());
			}
		}
		return $this->authToken;
	}

	public function sendData($data, $type = 'stock')
	{
		$url = ($type =='stock') ? $this->url.self::STOCK_URL : $this->url.self::PRICE_URL ;

		try{
			$response = CurlHelper::factory($url)
				->setHeaders($this->getHeaders())
				->setHeaders(['Authorization' => 'Bearer '.$this->authToken])
				->setPostFields($data)
				->exec();
		} catch(Exception $e) {
			$response['status'] = 500;
			$response['content'] = $e->getMessage();
		}

		return $response;
	}
}