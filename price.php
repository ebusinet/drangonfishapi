<?php
require_once 'vendor/autoload.php';
require_once 'Dragonfish.php';
require_once 'UConnector.php';
use Firebase\JWT\JWT;
use IDCT\Networking\Ssh\SftpClient;
use IDCT\Networking\Ssh\Credentials;

const SYNC_NAME = 'price';
const COL_PLATFORM_ID = 0;
const COL_SKU = 1;
const COL_PRICE = 2;

$start = microtime(true);
echo SYNC_NAME." data sync start. \n";

$dragonfish = new Dragonfish();
$jwt = $dragonfish->getAuthToken();
var_dump($jwt);
if ($jwt){
	echo "Retriving ".SYNC_NAME." data from ERP. \n";
 	$data = $dragonfish->getDataFromErp(SYNC_NAME);
 	var_dump($data);
} else {
	var_dump($jwt);
}

if (!empty($data)) {
	echo "Sending ".SYNC_NAME." data to UConnector. \n";

	$timestamp = date('Y-m-d_H_i_s', time());
	// array_unshift($data,['sku', 'price']); no header
	$localFilePath = $dragonfish->createCSV(SYNC_NAME, $timestamp, $data);
}
if ($localFilePath) {
	$remotePath = '/import/price/pending/';
	$host = 'uconnector.ulula.net';
	$port = 22;
	$timeout = 10;
	$user = 'spaceball';
	$pass = 'spaceball2021';
	$client = new SftpClient();
	$credentials = Credentials::withPassword($user, $pass);
	$client->setCredentials($credentials);

	$client->connect($host);
	$client->upload($localFilePath, $remotePath.SYNC_NAME.'.csv');
	unlink ($localFilePath);
}


echo SYNC_NAME." data sync finished. \n";
$time_elapsed_secs = microtime(true) - $start;
echo "Time to execute: ".$time_elapsed_secs ." seconds \n";