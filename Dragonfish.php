<?php
require_once 'vendor/autoload.php';
require_once 'config.php';
use Firebase\JWT\JWT;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * 
 */
class Dragonfish
{
	const AUTH_URL = 'Autenticar';
	const PRODUCTS_URL = 'Articulo';
	const STOCK_URL = 'Tiinventariofisico/';
	const PRICE_URL = 'Listadeprecios/';
	const ORDER_URL = 'Operacionecommerce/';
	const STOCK_PRICE = 'ConsultaStockYPrecios/';
	const LIMIT = 1000;
	const STOCK_CERO = 'true';
	const SKU_SEPARATOR = '!';

	protected $key;
	protected $name;
	protected $user;
	protected $password;
	protected $url;
	protected $jwToken;
	protected $authToken;
	protected $priceLists;
	protected $logger;

	function __construct()
	{
		$config = Config::getConfig();
		$this->key = $config['dragonfish']['client_key'];
		$this->name = $config['dragonfish']['client_name'];
		$this->user = $config['dragonfish']['user'];
		$this->password = $config['dragonfish']['password'];
		$this->url = $config['dragonfish']['host'].':'.$config['dragonfish']['port'].'/api.Dragonfish/';
		$this->priceLists = $config['dragonfish']['price_list'];
		$this->logger = new Logger('dragonfish');
		$this->logger->pushHandler(new StreamHandler('log/dragonfish.log', Logger::WARNING));
	}

	public function getJWToken()
	{
		if (!$this->jwToken)
			$this->jwToken = JWT::encode($this->getToken(), $this->key);
		return $this->jwToken;
	}

	public function getToken()
	{
		$time = time();
		return [
		    'exp' => $time + (60*60), // Tiempo que expirará el token (+1 hora)
		    'usuario' => $this->user,
		    'password' => $this->password
		];
	}

	protected function getHeaders()
	{
		$headers =  [
				'Content-Type' => 'application/json',
				'Accept' => 'application/json'
			];
		if ($this->authToken) {
			$headers['Authorization'] = $this->authToken;
			$headers['IdCliente'] = $this->name;
		} 
		return $headers;
	}

	protected function getAuthBody()
	{
		return [
			'IdCliente' => $this->name,
			'JWToken' => $this->getJWToken()
		];
	}

	public function getAuthToken()
	{
		$url = $this->url.self::AUTH_URL;
		if (!$this->authToken) {
			try{
				$response = CurlHelper::factory($url)
					->setHeaders($this->getHeaders())
					->setPostFields($this->getAuthBody())
					->exec();
				if ($response['status'] == 200) {
					$this->authToken = $this->getJWToken();
				}
				else {
					return $response;
				}
			} catch(Exception $e) {
				print_r($e->getMessage());
			}
		}
		return $this->authToken;
	}

	public function callAPi($apiName, $priceList, $page)
	{
		$url =  $this->url.$apiName;
		return CurlHelper::factory($url)
					->setHeaders($this->getHeaders())
					->setGetParams([
						'limit'=> self::LIMIT,
						'page' => $page,
						'stockcero' => self::STOCK_CERO,
						'lista' =>  $priceList
					])
					->exec();
	}

	public function getDataFromErp($type, $ismock=false)
	{
		$data = [];
		$i = 0;
		foreach ($this->priceLists as $platformId => $priceList) {
			if ($type == 'stock' && $i > 0) break;

			$data = $this->getProductData($type, $data, $platformId, $priceList, null, $ismock);
			$i++;
		}

		return $data;
	}

	public function createOrder($order)
	{
		try{
			$url =  $this->url.self::ORDER_URL;
			$response = CurlHelper::factory($url)
					->setHeaders($this->getHeaders())
					->setPostRaw($order)
					->exec();
			if ($response['status'] == 201) {
				return true;
			} else {
				$this->logger->error($response['data']);
				return false;
			}
		} catch(Exception $e) {
				print_r($e->getMessage());
		}
		
	}

	public function getProductData($type, $data, $platformId, $priceList, $page=1, $ismock=false)
	{
		try{
				echo "page ".$page. "\n";
				if ($ismock) {
					$response = json_decode($this->getMockedResponse(), true);
				} else {
					$response = $this->callAPi(self::STOCK_PRICE, $priceList, $page);
				}

				if ($response['status'] == 200) {
					$results = $response['data']['Resultados'];
					$data = $this->setData($type, $data , $platformId, $results);

					if ($response['data']['Siguiente'] && !$ismock) {
						$page++;
						$data = $this->getProductData($type, $data, $platformId, $priceList, $page);
					}
			
				} else {
					$this->logger->error($response['data']);
				}

			return $data;
			
			} catch(Exception $e) {
				print_r($e->getMessage());
		}
	}

	protected function setData($type, $data, $platformId, $products) 
	{
		foreach ($products as $product) {
			$sku = $this->getSku($type, $product);
			if (!$sku) continue;
			$data[] = ($type == 'stock') ?
				[$sku, $this->getQty($product)] :
				[$sku, $product['Precio']];
		}
		return $data;
	}

	protected function getSku($type, $product)
	{
		if ( ((empty($product['Color']) || empty($product['Talle'])) && $type == 'stock'))
			return false;

		if ( ((!empty($product['Color']) || !empty($product['Talle'])) && $type == 'price'))
			return false;

		if ($type == 'price') return trim($product['Articulo']);

		return trim($product['Articulo'])
			.self::SKU_SEPARATOR
			.$product['Color']
			.self::SKU_SEPARATOR
			.$product['Talle'];
	}

	protected function getQty($product)
	{
		return $product['Stock'];
	}

	public function createCSV($type, $timestamp, $data)
	{
		$path = getcwd();
		$dir = $path.DIRECTORY_SEPARATOR.$type.DIRECTORY_SEPARATOR;
		if (!is_dir($dir)) {
		    mkdir($dir, 0777, true);
		}

		// $fp = fopen($dir.DIRECTORY_SEPARATOR.$type.'-'.$timestamp.'.csv', 'w');
		$fp = fopen($dir.DIRECTORY_SEPARATOR.$type.'.csv', 'w');
		if ($fp) {
			foreach ($data as $fields) {
			    fputcsv($fp, $fields);
			}

			fclose($fp);
			return $dir.DIRECTORY_SEPARATOR.$type.'.csv';
		} else {
			echo $dir. ' is not writable o directory do not exists';
		}
		
		return false;
	}

	protected function getMockedResponse()
	{
		return '{"status":200,"type":"application\/json; charset=utf-8","headers":{"Access-Control-Allow-Headers":"content-type,Authorization,IdCliente","Access-Control-Allow-Methods":"OPTIONS,GET,POST,DELETE,PUT","Access-Control-Allow-Origin":null,"Access-Control-Expose-Headers":"InformacionAdicional","Content-Length":"2046","Content-Type":"application\/json; charset=utf-8","Date":"Tue, 21 Apr 2020 13:58:16 GMT","Server":"Microsoft-HTTPAPI\/2.0"},"cookies":[],"headers_raw":"HTTP\/1.1 200 OK\r\nContent-Length: 2046\r\nContent-Type: application\/json; charset=utf-8\r\nServer: Microsoft-HTTPAPI\/2.0\r\nAccess-Control-Allow-Origin: \r\nAccess-Control-Allow-Headers: content-type,Authorization,IdCliente\r\nAccess-Control-Expose-Headers: InformacionAdicional\r\nAccess-Control-Allow-Methods: OPTIONS,GET,POST,DELETE,PUT\r\nDate: Tue, 21 Apr 2020 13:58:16 GMT\r\n\r\n","content":"{\"CountResultado\":10,\"Siguiente\":\"http:\\\/\\\/localhost:8008\\\/api.Dragonfish\\\/ConsultaStockYPrecios\\\/?limit=10&stockcero=true&lista=LISTA1&page=2\",\"TotalRegistros\":6827,\"Resultados\":[{\"Articulo\":\"BH8101\",\"ArticuloDescripcion\":\"JOGG TIRA INTERNA\",\"Color\":\"\",\"ColorDescripcion\":null,\"Lista\":\"LISTA1\",\"Precio\":465.0,\"Stock\":0.0000,\"Talle\":\"\",\"TalleDescripcion\":null},{\"Articulo\":\"BH8101\",\"ArticuloDescripcion\":\"JOGG TIRA INTERNA\",\"Color\":\"02\",\"ColorDescripcion\":\"Negro\",\"Lista\":\"LISTA1\",\"Precio\":465.0,\"Stock\":0.0000,\"Talle\":\"L\",\"TalleDescripcion\":\"L\"},{\"Articulo\":\"BH8101\",\"ArticuloDescripcion\":\"JOGG TIRA INTERNA\",\"Color\":\"02\",\"ColorDescripcion\":\"Negro\",\"Lista\":\"LISTA1\",\"Precio\":465.0,\"Stock\":7.0,\"Talle\":\"M\",\"TalleDescripcion\":\"M\"},{\"Articulo\":\"BH8101\",\"ArticuloDescripcion\":\"JOGG TIRA INTERNA\",\"Color\":\"02\",\"ColorDescripcion\":\"Negro\",\"Lista\":\"LISTA1\",\"Precio\":465.0,\"Stock\":46.0,\"Talle\":\"S\",\"TalleDescripcion\":\"S\"},{\"Articulo\":\"BH92559\",\"ArticuloDescripcion\":\"BERMUDA CARGO ANDY\",\"Color\":\"\",\"ColorDescripcion\":null,\"Lista\":\"LISTA1\",\"Precio\":2405.0,\"Stock\":0.0000,\"Talle\":\"\",\"TalleDescripcion\":null},{\"Articulo\":\"BH92559\",\"ArticuloDescripcion\":\"BERMUDA CARGO ANDY\",\"Color\":\"02\",\"ColorDescripcion\":\"Negro\",\"Lista\":\"LISTA1\",\"Precio\":2405.0,\"Stock\":1.0,\"Talle\":\"28\",\"TalleDescripcion\":\"28\"},{\"Articulo\":\"BH92559\",\"ArticuloDescripcion\":\"BERMUDA CARGO ANDY\",\"Color\":\"02\",\"ColorDescripcion\":\"Negro\",\"Lista\":\"LISTA1\",\"Precio\":2405.0,\"Stock\":0.0000,\"Talle\":\"30\",\"TalleDescripcion\":\"30\"},{\"Articulo\":\"BH92559\",\"ArticuloDescripcion\":\"BERMUDA CARGO ANDY\",\"Color\":\"02\",\"ColorDescripcion\":\"Negro\",\"Lista\":\"LISTA1\",\"Precio\":2405.0,\"Stock\":5.0,\"Talle\":\"32\",\"TalleDescripcion\":\"32\"},{\"Articulo\":\"BH92559\",\"ArticuloDescripcion\":\"BERMUDA CARGO ANDY\",\"Color\":\"02\",\"ColorDescripcion\":\"Negro\",\"Lista\":\"LISTA1\",\"Precio\":2405.0,\"Stock\":12.0,\"Talle\":\"34\",\"TalleDescripcion\":\"34\"},{\"Articulo\":\"BH92559\",\"ArticuloDescripcion\":\"BERMUDA CARGO ANDY\",\"Color\":\"02\",\"ColorDescripcion\":\"Negro\",\"Lista\":\"LISTA1\",\"Precio\":2405.0,\"Stock\":0.0000,\"Talle\":\"36\",\"TalleDescripcion\":\"36\"}]}","data":{"CountResultado":10,"Siguiente":"http:\/\/localhost:8008\/api.Dragonfish\/ConsultaStockYPrecios\/?limit=10&stockcero=true&lista=LISTA1&page=2","TotalRegistros":6827,"Resultados":[{"Articulo":"BH8101","ArticuloDescripcion":"JOGG TIRA INTERNA","Color":"","ColorDescripcion":null,"Lista":"LISTA1","Precio":465,"Stock":0,"Talle":"","TalleDescripcion":null},{"Articulo":"BH8101","ArticuloDescripcion":"JOGG TIRA INTERNA","Color":"02","ColorDescripcion":"Negro","Lista":"LISTA1","Precio":465,"Stock":0,"Talle":"L","TalleDescripcion":"L"},{"Articulo":"BH8101","ArticuloDescripcion":"JOGG TIRA INTERNA","Color":"02","ColorDescripcion":"Negro","Lista":"LISTA1","Precio":465,"Stock":7,"Talle":"M","TalleDescripcion":"M"},{"Articulo":"BH8101","ArticuloDescripcion":"JOGG TIRA INTERNA","Color":"02","ColorDescripcion":"Negro","Lista":"LISTA1","Precio":465,"Stock":46,"Talle":"S","TalleDescripcion":"S"},{"Articulo":"BH92559","ArticuloDescripcion":"BERMUDA CARGO ANDY","Color":"","ColorDescripcion":null,"Lista":"LISTA1","Precio":2405,"Stock":0,"Talle":"","TalleDescripcion":null},{"Articulo":"BH92559","ArticuloDescripcion":"BERMUDA CARGO ANDY","Color":"02","ColorDescripcion":"Negro","Lista":"LISTA1","Precio":2405,"Stock":1,"Talle":"28","TalleDescripcion":"28"},{"Articulo":"BH92559","ArticuloDescripcion":"BERMUDA CARGO ANDY","Color":"02","ColorDescripcion":"Negro","Lista":"LISTA1","Precio":2405,"Stock":0,"Talle":"30","TalleDescripcion":"30"},{"Articulo":"BH92559","ArticuloDescripcion":"BERMUDA CARGO ANDY","Color":"02","ColorDescripcion":"Negro","Lista":"LISTA1","Precio":2405,"Stock":5,"Talle":"32","TalleDescripcion":"32"},{"Articulo":"BH92559","ArticuloDescripcion":"BERMUDA CARGO ANDY","Color":"02","ColorDescripcion":"Negro","Lista":"LISTA1","Precio":2405,"Stock":12,"Talle":"34","TalleDescripcion":"34"},{"Articulo":"BH92559","ArticuloDescripcion":"BERMUDA CARGO ANDY","Color":"02","ColorDescripcion":"Negro","Lista":"LISTA1","Precio":2405,"Stock":0,"Talle":"36","TalleDescripcion":"36"}]},"xpath":[]}';
	}
}